﻿using System;

namespace Class01_Basic_Syntax
{
	public class HelloWorld
	{
	//    introduce main method

		public static void Main(string[] args)
		{
			sbyte b = 17; //1 byte    Stores whole numbers from -128 to 127
			short sh = 32212; //2 bytes    Stores whole numbers from -32,768 to 32,767
			int @in = 5474; //4 bytes    Stores whole numbers from -2,147,483,648 to 2,147,483,647
            //@ is to use reserved word for variable name

			long l = 245; //8 bytes    Stores whole numbers from -9,223,372,036,854,775,808 to 9,223,372,036,854,775,808
			float f = 32.3f; //4 bytes    Stores fractional numbers from 3.4e−038 to 3.4e+038. Sufficient for storing 6 to 7 decimal digits
			double d = 53.12d; //8 bytes    Stores fractional numbers from 1.7e−308 to 1.7e+038. Sufficient for storing 15 decimal digits
			bool @bool = true; //1 byte    Stores true or false values
			char ch = 'B'; //2 bytes    Stores a single character/letter
			string str = "Hello Java"; //double quotes vs. single quotes
			Console.Write(@bool);
			Console.Write(d);
			Console.WriteLine(str);
			Console.WriteLine(ch);

			Console.WriteLine("**************************");
			int x = 51, y = 23;
			Console.WriteLine(x + y);
			Console.WriteLine(x - y);
			Console.WriteLine(x * y);
			Console.WriteLine(x / y);
			Console.WriteLine(x % y);
            //System.out.println(x++);
            //System.out.println(++x);
            //System.out.println(x);
            x += y;
			x -= y;
			x *= y;
			x /= y;
			x %= y;

			Console.WriteLine("**************************");
			string s1, s2;
			s1 = "Hello";
			s2 = "Java";
			Console.WriteLine(s1 + s2);
			string s3 = s1 + "_" + s2;
			Console.WriteLine(s3);
			int z = 15;
			Console.WriteLine(z + s3);

			Console.WriteLine("**************************");
			//Scanner sc = new Scanner(System.in);
			Console.Write("Enter your name: ");
			string myName = Console.ReadLine(); //Wait for user input, naming convention
			Console.WriteLine("Good morning " + myName);

			Console.WriteLine("Enter your age: ");
			int age = Int32.Parse(Console.ReadLine());
			Console.WriteLine("Enter your height in meter: ");
			double height = Double.Parse(Console.ReadLine());
			
			Console.WriteLine("age: " + age + ", height: " + height);

			Console.WriteLine("**************************");
			string rank;
			Console.WriteLine("Enter your Math mark: ");
			double mathMark = Double.Parse(Console.ReadLine());

            if (mathMark >= 9.0)
			{ // == != > < >= <= && || !
				rank = "Excellent";
			}
			else if (mathMark >= 8.0)
			{
				rank = "Good";
			}
			else if (mathMark >= 6.5)
			{
				rank = "Well Done";
			}
			else if (mathMark >= 5.0)
			{
				rank = "Average";
			}
			else
			{
				rank = "Unsatisfied";
			}
			Console.WriteLine(rank);

			Console.WriteLine("Enter a number: ");
			int day = Int32.Parse(Console.ReadLine());
            switch (day)
			{
				case 1:
					Console.WriteLine("Monday");
					break;
				case 2:
					Console.WriteLine("Tuesday");
					break;
				case 3:
					Console.WriteLine("Wednesday");
					break;
				case 4:
					Console.WriteLine("Thursday");
					break;
				case 5:
					Console.WriteLine("Friday");
					break;
				case 6:
					Console.WriteLine("Saturday");
					break;
				case 7:
					Console.WriteLine("Sunday");
					break;
				default:
					Console.WriteLine("Unknown!");
					break;
			}

			Console.WriteLine("**************************");
			int n = 0;
			while (n < 5)
			{
				Console.WriteLine(n);
				n++;
			}

			n = 0;
			do
			{
				Console.WriteLine(n);
				n++;
			} while (n < 5);

			for (int i = 0; i < 5; i++)
			{
				Console.WriteLine(i);
			}

			Console.WriteLine("**************************");
			string[] cars = new string[] {"Volvo", "BMW", "Ford", "Mazda"};
			Console.WriteLine(cars[0]);
			cars[1] = "Opel";
			Console.WriteLine(cars[1]);
			Console.WriteLine("#cars: " + cars.Length);

			for (int i = 0; i < cars.Length; i++)
			{
				Console.WriteLine(cars[i]);
			}

			foreach (string c in cars)
			{
				Console.WriteLine(c);
			}

			int[][] myNumbers = new int[][]
			{
				new int[] {1, 2, 3, 4},
				new int[] {15, 16, 17}
			};
			int num = myNumbers[1][2];
			Console.WriteLine(num);

			for (int i = 0; i < myNumbers.Length; ++i)
			{
				for (int j = 0; j < myNumbers[i].Length; ++j)
				{
					Console.WriteLine(myNumbers[i][j]);
				}
			}

			Console.WriteLine("**************************");
			string txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			Console.WriteLine("The length of the txt string is: " + txt.Length);
			Console.WriteLine(txt[0]);
			Console.WriteLine(txt[15]);

			txt = "Hello World";
			Console.WriteLine(txt.ToUpper()); // Outputs "HELLO WORLD"
			Console.WriteLine(txt.ToLower()); // Outputs "hello world"

			txt = "Please locate where 'locate' occurs!";
			Console.WriteLine(txt.IndexOf("locate")); // Outputs 7



            //Math
            //https://www.w3schools.com/java/java_math.asp
	


			Console.WriteLine("**************************");
			double len = 12.2, wid = 5.3;
			double area = rectangleArea(len, wid);
			Console.WriteLine(area);
            //Console.WriteLine(rectangleArea(len, wid));

            checkPrimeNumber(10);
		}

		//exp: function, parameter, return type
		static double rectangleArea(double length, double width)
		{
			return length * width;
		}


		static void checkPrimeNumber(int n)
		{
			int sqrt = (int) Math.Sqrt(n);
			for (int i = 2; i <= sqrt; i++)
			{
				if (n % i == 0)
				{
					Console.WriteLine(n + " is NOT a prime number");
					return;
				}
			}
			Console.WriteLine(n + " is a PRIME number");
		}
	}

}